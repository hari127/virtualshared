const videoElement = document.getElementsByClassName('input_video')[0];
const canvasElement = document.getElementsByClassName('output_canvas')[0];
const canvasCtx = canvasElement.getContext('2d');


var posedata,poseString,faceString,facedata,weightLe,weightRe;
var a,b,c,d,e,f,g,h,i;



function onResults(results) {
  canvasCtx.save();
  canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
  //canvasCtx.drawImage(results.segmentationMask, 0, 0,canvasElement.width, canvasElement.height);

  // Only overwrite existing pixels.
  canvasCtx.globalCompositeOperation = 'source-in';
  canvasCtx.fillStyle = '#00FF00';
  canvasCtx.fillRect(0, 0, canvasElement.width, canvasElement.height);

  // Only overwrite missing pixels.
  canvasCtx.globalCompositeOperation = 'destination-atop';
  //canvasCtx.drawImage(
    //  results.image, 0, 0, canvasElement.width, canvasElement.height);
    
  canvasCtx.globalCompositeOperation = 'source-over';
  
  
  
  
  //console.log(poseString);
if(results.ea != undefined)
{
  a = results.ea[16].x;
  b =results.ea[16].y;
  c =results.ea[16].z;
  d = results.ea[15].x;
  e = results.ea[15].y;
  f = results.ea[15].z;
  g = results.ea[0].x;
  h = results.ea[0].y;
  i = results.ea[0].z;
  
  posedata = { a : a, b : b, c : c, d:d , e:e, f:f, g:g, h:h, i:i};
}


  const keypoints = results.faceLandmarks
if(keypoints !=undefined)
{
  const x0  =  results.faceLandmarks[13].x
  const y0  =  results.faceLandmarks[13].y
  

  const x1  =  results.faceLandmarks[14].x
  const y1  =  results.faceLandmarks[14].y

  const dist = distance(x0,y0,x1,y1);
   var openclosedist = normDist(dist);
   //console.log(openclosedist);
 ////console.log(results.faceLandmarks[13]+"                 "+results.faceLandmarks[14])
 ////console.log(openclosedist);

  var a0  =  results.faceLandmarks[384].x
  var b0  =  results.faceLandmarks[384].y

  var a1  =  results.faceLandmarks[381].x
  var b1  =  results.faceLandmarks[381].y
  const lefteye = distance(a0,b0,a1,b1);
  var leye = normeye(lefteye);
  if(leye<=100)
  {
    weightLe=1;
  }
  // if(leye<=7)
  // {
  //   weightLe = 0.6;
  // }
  if(leye<=5){
    weightLe = 0;
  }
  //console.log(weightLe)


   a0  =  results.faceLandmarks[157].x
   b0  =  results.faceLandmarks[157].y

   a1  =  results.faceLandmarks[154].x
   b1  =  results.faceLandmarks[154].y
  const righteye = distance(a0,b0,a1,b1);
  
  var reye = normeye(righteye,8);
  if(reye<=100)
  {
    weightRe=1;
  }
  // if(reye <=7)
  // {
  //   weightRe = 0.6;
  // }
  if(reye <=5){
    weightRe = 0;
  }
  //console.log(weightRe);
  ////console.log(results.faceLandmarks[61])
  ////console.log(results.faceLandmarks[291])
  var [
  verticalCos,
  horizontalCos
] = getHeadAnglesCos(keypoints)

  //console.log(weightRe)
//var head=char.model.model.graph.findByName("Head");
//char.model.model.morphInstances[0].setWeight(10)
verticalCos = Math.trunc( verticalCos );
horizontalCos = Math.trunc( -horizontalCos );
horizontalCos = horizontalCos+20;
//console.log(verticalCos-100,horizontalCos+90,0);
facedata = { mouthopen : openclosedist, vert : verticalCos, horiz : horizontalCos};
}
faceString = JSON.stringify(facedata);
poseString = JSON.stringify(posedata);

//console.log(poseString);
//console.log(faceString);
//console.log(verticalCos-100,horizontalCos+90,0);

  //console.log(faceString);

if(unityGame != undefined)
{
  //unityGame.SendMessage('JavascriptHook','PoseJson',poseString);
  unityGame.SendMessage('JavascriptGo','PoseCall',poseString);
  unityGame.SendMessage('JavascriptGo','FaceCall',faceString);
}
  //unityGame.SendMessage('JavascriptHook','RigFaceJson',faceString);
  
  drawConnectors(canvasCtx, results.poseLandmarks, POSE_CONNECTIONS,
                 {color: '#00FF00', lineWidth: 4});
  drawLandmarks(canvasCtx, results.poseLandmarks,
                {color: '#FF0000', lineWidth: 2});
  drawConnectors(canvasCtx, results.faceLandmarks, FACEMESH_TESSELATION,
                 {color: '#C0C0C070', lineWidth: 1});
  drawConnectors(canvasCtx, results.leftHandLandmarks, HAND_CONNECTIONS,
                 {color: '#CC0000', lineWidth: 5});
  drawLandmarks(canvasCtx, results.leftHandLandmarks,
                {color: '#00FF00', lineWidth: 2});
  drawConnectors(canvasCtx, results.rightHandLandmarks, HAND_CONNECTIONS,
                 {color: '#00CC00', lineWidth: 5});
  drawLandmarks(canvasCtx, results.rightHandLandmarks,
                {color: '#FF0000', lineWidth: 2});
  canvasCtx.restore();
}


function getHeadAnglesCos(keypoints) {
// V: 10, 152; H: 226, 446
var faceVerticalCentralPoint = [
0,
(keypoints[10].y + keypoints[152].y) * 0.5,
(keypoints[10].z + keypoints[152].z) * 0.5,
];
const verticalAdjacent =
keypoints[10].z - faceVerticalCentralPoint[2];
const verticalOpposite =
keypoints[10].y - faceVerticalCentralPoint[1];
const verticalHypotenuse = l2Norm([
verticalAdjacent,
verticalOpposite,
]);
const verticalCos = verticalAdjacent / verticalHypotenuse;

var faceHorizontalCentralPoint = [
  (keypoints[226].x + keypoints[446].x) * 0.5,
  0,
  (keypoints[226].z + keypoints[446].z) * 0.5,
];
const horizontalAdjacent =
        keypoints[226].z - faceHorizontalCentralPoint[2];
const horizontalOpposite =
        keypoints[226].x - faceHorizontalCentralPoint[0];
const horizontalHypotenuse = l2Norm([
  horizontalAdjacent,
  horizontalOpposite,
]);
const horizontalCos = horizontalAdjacent / horizontalHypotenuse;

var vertAngle = inCos(verticalCos)
var horizAngle = inCos(horizontalCos)

return [
  vertAngle,
  horizAngle,
];
}



function normDist(num) {
  num=(num/60)*100;
  return num
}

function normeye(num) {
  num=(num/6)*10;
  //console.log("Number is"+num )
  num = Math.trunc(num);
  return num;
}

function distance(x0, y0, x1, y1)
{
 return Math.hypot(x1 - x0, y1 - y0);
}

function l2Norm(vec) {
    let norm = 0;
    for (const v of vec) {
    norm += v * v;
    }
    return Math.sqrt(norm);
    }  

    function inCos(value)
    {
     var radians = Math.acos(value)
      var pi=Math.PI
      var degrees = radians * (180/pi);
      return degrees
    }





const holistic = new Holistic({locateFile: (file) => {
  return `https://cdn.jsdelivr.net/npm/@mediapipe/holistic/${file}`;
}});
holistic.setOptions({
  modelComplexity: 1,
  smoothLandmarks: true,
  enableSegmentation: true,
  smoothSegmentation: true,
  refineFaceLandmarks: true,
  minDetectionConfidence: 0.5,
  minTrackingConfidence: 0.5
});
holistic.onResults(onResults);

const camera = new Camera(videoElement, {
  onFrame: async () => {
    await holistic.send({image: videoElement});
  },
  width: 1280,
  height: 720
});
camera.start();
